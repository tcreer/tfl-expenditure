from django.conf.urls import patterns, include, url

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    url(r'^$', 'dataentry.views.home', name='home'),
    # url(r'^tflex/', include('tflex.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    #url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    #url(r'^admin/', include(admin.site.urls)),
    
    url(r'^query', 'dataentry.views.filter_page', name='filter_page'),
    url(r'^xls', 'dataentry.views.return_xls', name='return_xls'),
    url(r'^feedback', 'dataentry.views.ret_feedback', name='ret_feedback'),
    url(r'^graph', 'dataentry.views.return_graph', name='return_graph'),
)
