# Create your views here.
import socket
from django.http import HttpResponse, Http404, HttpResponseNotAllowed

from django.shortcuts import render_to_response
from django.shortcuts import render
from django.db.models import Sum
import datetime
from django.views.decorators.csrf import csrf_exempt

from dataentry.models import RowEntry, FileEntry

from django.utils.encoding import smart_str
def ret_feedback(request):
	#try:
		send_feedback(request.GET.get('n','')+'\r\n\r\n'+\
		request.GET.get('e','')+'\r\n\r\n'+\
		request.GET.get('m',''),subject="TfL expenditure - feedback")
		
		return HttpResponse('Feedback sent, thanks! <a href="javascript:history.back()">Back</a>', content_type="text/html")
		
	#except:
	#	return HttpResponse('Sorry, sending feedback failed. Please try again later. <a href="javascript:history.back()">Back</a>', content_type="text/html")
	
def home(request):
	try:
		cstring = request.META['QUERY_STRING']
	except:
		cstring = ''
	return render_to_response('index.html',{'cstring':cstring,
	'com':request.GET.get('c',''),
	'doc':request.GET.get('n',''),
	'exp':request.GET.get('ex',''),
	'mer':request.GET.get('cat',''),
	'ent':request.GET.get('e',''),
	'amo':request.GET.get('a',''),
	'ao':request.GET.get('ao',''),
	'dat':request.GET.get('d',''),
	'd_o':request.GET.get('do',''),
	})
	#return render_to_response('debuganswers.html', {'r' : r})

from dateutil.parser import parse
from django.core import serializers
#from django.utils import simplejson

def filter(request,xlsf=''):
	#take inputs
	kwargs = {}
	kwargs['vendor_name__icontains'] = request.GET.get('c','')
	kwargs['doc_num__istartswith'] = request.GET.get('n','')
	kwargs['merchant_cat'] = request.GET.get('cat','')
	kwargs['entity__icontains'] = request.GET.get('e','')
	kwargs['exp_account__icontains'] = request.GET.get('ex','')
	
	sort = request.GET.get('sort','clearing_date')
	order = request.GET.get('order','-')
	if order == 'asc':
		order = ''
	else:
		order ='-'
	
	if xlsf=='graph':
		sort = 'clearing_date'
		order =''
	
	op_key = {'1':'','2':'__gte','3':'__lte'}
	
	try :
		d_op = op_key[request.GET.get('do','')]
	except KeyError:
		d_op = ''
		
	try :
		a_op = op_key[request.GET.get('ao','')]
	except KeyError:
		a_op = ''

	#add joins (__)
	try:
		amount =request.GET.get('a','')
		if amount != '':
			kwargs['amount'+a_op] = float(amount.replace(',',''))
	except:
		pass
	date = request.GET.get('d','')
	if date != '':
		kwargs['clearing_date'+d_op] = parse(date)
	
	#filter out blank keys:
	kwargs = {k: v for k, v in kwargs.items() if v}
	
	if len(kwargs) > 0:
		r = RowEntry.objects.filter(**kwargs).order_by(order+sort)
		r_sql = r.query #use later on
		#print r_sql
	else:
		if xlsf == '':
			r = RowEntry.objects.all().order_by(order+sort)
		else:
			return 0
		
	return r



import json as simplejson
def filter_page(request):
	r = filter(request)

	try: #may receive bad info
		num_per_page = int(request.GET.get('limit',20))
		offset = int(request.GET.get('offset',0))
	except:
		num_per_page = 20
		offset = 0

	if r.count() == 0:
		l_data = '{"total": 0, "rows": [{}]}'
		return HttpResponse(l_data, content_type="application/json")

	q_t  = r.count()-1
	if q_t == -1:
		return HttpResponse('', content_type="application/json")
	
	if offset > q_t:
		offset = q_t
	elif offset < 0:
		offset = 0
	
	amount_total = r.aggregate(Sum('amount'))['amount__sum']
	
	r = r[offset:min(q_t+1,offset+num_per_page)]
	
	page_total = 0
	for r1 in r:
		page_total += r1.amount
	#print r.query
	
	l_data = [{'exp_account': o.exp_account,'entity': o.entity,\
	'merchant_cat': o.merchant_cat,'vendor_name': o.vendor_name,'doc_num': o.doc_num,\
	'clearing_date': str(o.clearing_date)[0:10],'amount': '%.2f'%o.amount,
	} for o in r]
	
	
	l_data.append({'exp_account':'<i>PAGE TOTAL</i>','amount':'<i>'+"{:,.2f}".format(page_total)+'</i>'})
	l_data.append({'exp_account':'<b>GRAND TOTAL</b>','amount':'<b>'+"{:,.2f}".format(amount_total)+'</b>'})
	#l_data.append({'exp_account':'GRAND TOTAL','amount':amount_total})
	
	k_data = {}
	k_data['rows']=l_data
	k_data['total'] = q_t+1
	
	#total = 0 
	#for o in r:
	#	total=+o.amount
	#k_data.append({'total':q_t,'pages':q_n,'total_amount':total})
	
	data = simplejson.dumps(k_data)

	#data = serializers.serialize("json",r)
	return HttpResponse(data, content_type="application/json")


	
import xlwt
def return_xls(request):
	r = filter(request,'xls')

	if r:
		if r.count() > 65336:
			return HttpResponse('(Too many rows for Excel)', content_type="text/html")
		book = xlwt.Workbook(encoding='utf8')
		sheet = book.add_sheet('TfL expenses query')

		default_style = xlwt.Style.default_style
		datetime_style = xlwt.easyxf(num_format_str='dd/mm/yyyy hh:mm')
		date_style = xlwt.easyxf(num_format_str='dd/mm/yyyy')

		values_list = r.values_list()
		sheet.write(0,0,'Entity')
		sheet.write(0,1,'Merchant Category')
		sheet.write(0,2,'Date')
		sheet.write(0,3,'Amount')
		sheet.write(0,4,'Vendor Name')
		sheet.write(0,5,'Exp Account')
		sheet.write(0,6,'Doc num')
		
		for row, rowdata in enumerate(values_list):
			for col, val in enumerate(rowdata):
				if col > 1:
					if col == 4:
						try:
							sheet.write(row+1, col-2, val.replace(tzinfo=None),datetime_style)
						except:
							sheet.write(row+1, col-2, val)
					else:
						sheet.write(row+1, col-2, val)
		
		response = HttpResponse(content_type='application/vnd.ms-excel')
		response['Content-Disposition'] = 'attachment; filename=tflexpenses.xls'
		book.save(response)
		return response
	else:
		return HttpResponse('(Too many rows for Excel)', content_type="text/html")

from json import encoder
def return_graph(request):
	r = filter(request,'graph')
		
	if r:
		data = []
		values_list = r.values_list()
		x=0.00
	
	
		json = ''
		for row, rowdata in enumerate(values_list):
			x=x+float(rowdata[5])
			encoder.FLOAT_REPR = lambda o: format(o, '.2f') #fixes json float 2 d.p.
			datestr = str(rowdata[4])[0:10]
			json = json + '{"c":[{"v":"Date('+datestr[0:4]+','+datestr[5:7]+','+datestr[8:10]+')"},{"v":'+"{0:.2f}".format(x)+'},{"v":'+str(row)+'}]},'
			
		#json = simplejson.dumps(data)
		json = '{"cols":[{"type":"date"},{"type":"number"},{"type":"number","label":"Line"}],"rows":['+json+']}'
		return HttpResponse(json, content_type="application/json")
	else:
		return HttpResponse('', content_type="text/html")
		
import smtplib
def send_feedback(text,subject='',to_addr='tomcreer@gmail.com'):
	fromaddr = 'turkeygobble90@gmail.com'

	msg = "\r\n".join([
	  "From: user_me@gmail.com",
	  "To: user_you@gmail.com",
	  "Subject: "+subject,
	  "",
	  text
	  ])

	username = 'turkeygobble90@gmail.com'
	password = 'Password1_'
	server = smtplib.SMTP('smtp.gmail.com:587')
	server.ehlo()
	server.starttls()

	server.login(username,password)
	server.sendmail(fromaddr, to_addr, msg)
	server.quit()



	
	