# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models


#DATA > MODELS.PY

# Create your models here.
class FileEntry(models.Model):
	dateadded = models.DateTimeField(auto_now=True)
	fileName = models.CharField(max_length=150)
	filePath = models.CharField(max_length=300,blank=True)
	numRows = models.SmallIntegerField(default=0)
	
	def __unicode__(self):
		return self.fileName
	
class RowEntry(models.Model):
	dateadded = models.DateTimeField(auto_now=True)
	
	entity = models.CharField(max_length=300,blank=True)
	merchant_cat = models.CharField(max_length=300,blank=True)
	clearing_date = models.DateTimeField()
	amount = models.DecimalField(max_digits=20, decimal_places=2)
	vendor_name = models.CharField(max_length=300,blank=True)
	exp_account = models.CharField(max_length=300,blank=True)
	doc_num = models.CharField(max_length=300,blank=True)	
	
	def __unicode__(self):
		return self.vendor_name + u' ' + unicode(self.amount) + ' ' + unicode(self.clearing_date)


import StringIO
import csv, os, unicodecsv
from dateutil.parser import parse
import codecs
from django.utils.encoding import smart_str


def addFile(fn):
	fQ = FileEntry.objects.filter(fileName=fn)
	if fQ.count() > 0: #check if exists already
		return 0
	
	num_lines = sum(1 for line in open(fn))

	f = open(fn, 'rb')
	with open(fn, 'rb') as f:
		s1= f.read().decode('latin-1')
	
	#content = unicode(s1.strip(codecs.BOM_UTF8), 'utf-8')
	#parser.parse(StringIO.StringIO(content))
	s2 = smart_str(s1)
	
	s3 = StringIO.StringIO(s2)
	reader = unicodecsv.DictReader(s3, encoding='utf-8')
	
#Vendor Name,Expenditure Account ,Document Number,Clearing Date,Amount ()
#Entity,Vendor Name,Expenditure Account ,Document Number,Clearing Date,Amount (?),Merchant Category,


	row = u''
	x = 0
	flag = 0
	for row in reader:
		docnum = row['Document Number']
		if docnum.strip() == '':
			continue
			
			#commented out was for checking for previous entries (obvs slower)
		#~ if flag == 0 :
			#~ rQ = RowEntry.objects.filter(doc_num=docnum)
			#~ if len(rQ) > 0:
				#~ x += 1
				#~ print x
				#~ continue
		#~ else:
			#~ flag = 1		
		
		
		vname = row['Vendor Name']
		
		try:
			entity = row['Entity']
		except KeyError: #doesnt exist in all CSVs
			entity = ''
			
		#~ if (docnum.find('\\') > 0):
			#~ x += 1
			#~ print x
			#~ continue				
		#~ else:
			#~ print 'go'			
			
		try:
			merccat = row['Merchant Category']
		except KeyError: #doesnt exist in all CSVs
			merccat = ''
		amount = row[u'Amount ()']
		

		
		expacc = row['Expenditure Account ']
		cldate = row['Clearing Date']
		
		cldate = parse(cldate)
		am2 = amount.replace('\'','').replace(',','').strip()
		if am2[0] == '(':
			am = float(am2.replace('(','-').replace(')',''))
		else:
			am = float(am2)

		#	commented out below won't create duplicates (much slower though)
		#~ d,_ = RowEntry.objects.get_or_create(entity = entity, merchant_cat=merccat , clearing_date=cldate, \
		#~ amount =am, vendor_name = vname, exp_account = expacc, doc_num=docnum)

		d = RowEntry(entity = entity, merchant_cat=merccat , clearing_date=cldate, \
		amount =am, vendor_name = vname, exp_account = expacc, doc_num=docnum)
		d.save()

		x += 1
		
		print str(x) + ' / ' + str(num_lines)
		
	fE = FileEntry(fileName= fn, numRows= num_lines)
	fE.save()
	f.close()
	return 1
	
def runForFiles(dir):
	dir = '/home/tommo/projects/tflex/tflex/data'
	for fn in os.listdir(dir):
		if addFile(dir +'/' + fn):
			print 'y + ' + fn
		else:
			print 'n + ' + fn
			

		


