from django.contrib import admin
from dataentry.models import FileEntry, RowEntry

admin.site.register(FileEntry)
admin.site.register(RowEntry)